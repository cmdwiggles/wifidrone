#!/usr/bin/env python3

############################
#
#   This class is make a basic tx stream writer for the xbox controller
#   Handles basic consistent consistent tx frequency of messages sent
#   and basic error checking. TX radio callback function is used to send the data
#
############################


import threading
import select
import subprocess
import os
import time
import random
import hashlib
from xboxcnt.xboxdrv_wrapper import xboxdrv_reader
import sys

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from xboxcnt.xbox_config import SEND_ORDER

class Xbox_TX_Man(threading.Thread):
    def __init__(self,freq=100,tx_write_call=None,tx_write_call_channel=0):
        threading.Thread.__init__(self)
        self.daemon = True
        self.running = True

        self.xboxdrv_read_thread = None

        self.tx_write_call = tx_write_call
        self.tx_write_call_channel = tx_write_call_channel

        self.frequency = freq
        self._loop_time = 1.0 / freq
        self._delay_time = 0
        self.final_loop_time = 0

    def run(self):
        self.running = True
        self._start()



##################
#
#   Code to do the bulk of the tx
#   Controller first starts up an instance of an xboxdrv wrapper
#   Sets wait loops on tx frequency rate to send messages to drone
#
    def _start(self):
        self.xboxdrv_read_thread = xboxdrv_reader()
        self.xboxdrv_read_thread.start()
        tx_msg = ""
        while self.running:
            #   Start
            start_time = time.time()
            tx_msg = self.generate_control_string()
            if self.tx_write_call:
                self.tx_write_call((tx_msg+"\n"),(self.tx_write_call_channel),ascii=False)

            process_time = time.time() - start_time
            self._delay_time = self._loop_time - process_time

            if self._delay_time > 0 and self._delay_time < 0.1:
                time.sleep(self._delay_time)

            self.final_loop_time = time.time() - start_time
            #print(self.final_loop_time)



###################
#   Function to generate the required control string to send
#   simple
#
#
    def generate_control_string(self):
        tx_string = ""
        #for val_name in self.xboxdrv_read_thread.controls:
        for val_name in SEND_ORDER:
            if val_name is "idx":
                value = round(time.time() * 10000.0)
            else:
                value = self.xboxdrv_read_thread.controls[val_name]
            tx_string += (",%s"%(value))
        tx_string = tx_string.strip(',')
        tx_crc = hashlib.md5(tx_string.encode('utf-8')).hexdigest()

        tx_string = tx_string.lstrip(',') + "_" + tx_crc

        return tx_string



    def stop(self):
        self.running = False


    def __del__(self):
        self.stop()



if __name__ == "__main__":
    try:
        #test_tx = Xbox_TX_Man(tx_write_call=write_message)
        test_tx = Xbox_TX_Man()
        test_tx.start()

        time.sleep(10)

        test_tx.stop()


    #Ctrl C
    except KeyboardInterrupt:
            print("User cancelled")

        #error
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    finally:
        #stop the controller
        pass
        test_tx.stop()

