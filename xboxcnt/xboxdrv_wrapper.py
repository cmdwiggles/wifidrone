#!/usr/bin/env python3
import threading
import subprocess
import sys
import time
import random
from termcolor import colored

############################################
#   Typical servo mapping
# left = 1000
# middle = 1500
# right = 2000
############################################
#    Typical Mapping
#    X1    ROll/Aileron
#    Y1    Pitch/Elevator
#
#    X2    Yaw
#    
#    RT - LT = Throttle
#
# srv_neurtal =1500.0
# roll_div = 65.0
# pitch_div = 65.0
# yaw_div = 65.0
############################################

class xboxdrv_reader(threading.Thread):
    def __init__(self,kill_first_all=True):
        threading.Thread.__init__(self)
        self.daemon = True
        self.running = False
        self.fresh_lineread = False
        self.lineread = ""
        self.last_data = 0.0
        self.functional = True

        self.watchdog_class = xboxdrv_watchdog(self)

        self.expo_enable = True
        self.expo_controls = ['X1','X2','Y1','Y2']
    
        self.controls = {'X1':0, 'Y1':0,'X2':0,'Y2':0,\
            'du':0,'dd':0,'dl':0,'dr':0,\
            'back':0,'guide':0,'start':0,\
            'TL':0,'TR':0,\
            'A':0,'B':0,'X':0,'Y':0,\
            'LB':0,'RB':0,\
            'LT':0,'RT':0,
            'du_T':0,'dd_T':0,'dl_T':0,'dr_T':0,\
            'back_T':0,'guide_T':0,'start_T':0,\
            'TL_T':0,'TR_T':0,\
            'A_T':0,'B_T':0,'X_T':0,'Y_T':0,\
            'LB_T':0,'RB_T':0,\
            'idx':0,\
            'last_read':time.time()\
            }
        self.running = True
        self.last_data = time.time()
        if kill_first_all:
            subprocess.call(['killall','xboxdrv'])
            time.sleep(0.5)
        self.xboxdrv_ps = subprocess.Popen(['xboxdrv','--quiet','--detach-kernel-driver'],stdout=subprocess.PIPE)
        time.sleep(0.5)
        if not self.xboxdrv_ps.poll():
            self.xbox_pipe = self.xboxdrv_ps.stdout
            print("xboxdrv started")
            self.watchdog_class.start()
        else:
            self.xbox_pipe = None
            self.functional = False

    def run(self):
        self.running = True
        self._start()

    def _start(self):
        print("starting xboxdrv pipe reader")
        print("Control Expo enabled:\t%r"%self.expo_enable)
        while(self.running):
            self.lineread = str(self.xboxdrv_ps.stdout.readline()).strip()
            if "[ERROR]" in self.lineread:

                print(colored(self.lineread, 'red'))
                print("Error in xboxdrv, restarting...")


            elif len(self.lineread) > 10:
                self.line_arr = self.lineread.replace(':',' ').split()
                if len(self.line_arr) >= 41:
                    self.fresh_lineread = True
                    self.convert_line()
                    if self.expo_enable:
                        self.expo_calc()
                    self.last_data = time.time()
                    self.functional = True

    def _restart_ps(self):
        print("restarting xboxdrv")
        self.xboxdrv_ps.kill()
        time.sleep(0.1)
        if self.xboxdrv_ps.poll():
            print("Xboxdrv is dead")
        else:
            print("Xboxdrv sigterm sent")

        subprocess.call(['killall', 'xboxdrv'])
        time.sleep(0.2)
        self.xboxdrv_ps = subprocess.Popen(['xboxdrv', '--quiet', '--detach-kernel-driver'], stdout=subprocess.PIPE)

    def stop(self):
        self.running = False
        if self.xboxdrv_ps:
            print("killing xboxdrv thread")
            self.xboxdrv_ps.kill()
            if self.xboxdrv_ps.poll():
                print("Xboxdrv is dead")
            else:
                print("Xboxdrv sigterm sent")

    def __del__(self):
        self.stop()

    def convert_line(self):
        try:
            for idx in range(1,43,2):
                if idx == 1:
                    self.controls['X1'] = int(self.line_arr[idx])
                elif idx > 2 and idx < 8:
                    self.controls[self.line_arr[idx-1]] = int(self.line_arr[idx])
                elif idx > 36 and idx < 42:
                    self.controls[self.line_arr[idx -1]] = int(self.line_arr[idx].split('\\')[0])
                elif idx == 43:
                    self.controls[self.line_arr[idx - 1]] = int(self.line_arr[idx].split('\\')[0])
                else:
                    self.controls[self.line_arr[idx -1]] = int(self.line_arr[idx])
                    if self.controls[self.line_arr[idx -1]] == 1:
                        toggle_str = self.line_arr[idx -1] + '_T'
                        if self.controls[toggle_str] == 0:
                            self.controls[toggle_str] = 1
                        else:
                            self.controls[toggle_str] = 0
            self.controls['idx'] = round(time.time() * 10000.0)
            self.controls['last_read'] = time.time()
        except:
            print("ERROR:   failed to read xboxstr")

    def expo_calc(self):
        for expo_chan in self.expo_controls:
            if expo_chan in self.controls.keys():
                if self.controls[expo_chan] < 0:
                    self.controls[expo_chan] = (self.controls[expo_chan] * self.controls[expo_chan]) >> 15
                    self.controls[expo_chan] = self.controls[expo_chan] * -1
                else:
                    self.controls[expo_chan] = (self.controls[expo_chan] * self.controls[expo_chan]) >> 15


class xboxdrv_watchdog(threading.Thread):
    def __init__(self,parent_handle,stop_watching_time=10):
        threading.Thread.__init__(self)
        self.daemon = True
        self.running = False
        self.loop_time = 0.5
        self.last_data_cutoff = 2
        self.parent_handle = parent_handle
        self.stop_watch_time = stop_watching_time

    def run(self):
        self.running = True
        self._start()

    def _start(self):
        print("Starting XboxDrv Watchdog")
        start_time = time.time()
        while self.running:
            if (start_time + self.stop_watch_time) <= time.time():
                print(colored("Shutting down Xboxdrv watchdog", 'green'))
                self.running = False
                break
            data_age = time.time() - self.parent_handle.controls['last_read']
            if data_age >= self.last_data_cutoff:
                #print("Error, watchdog reported old xbox data %.2f, restarting..."%(data_age))
                print(colored(("Error, watchdog reported old xbox data %.2f, restarting..."%(data_age)), 'blue'))
                self.parent_handle._restart_ps()
                start_time = time.time()
            time.sleep(self.loop_time)

    def stop(self):
        self.running = False

    def __del__(self):
        self.stop()






if __name__ == "__main__":
    try:
        import hashlib
        #print("print python version:"+str(sys.version))
        xboxdrv_read_thread = xboxdrv_reader()
        xboxdrv_read_thread.start()
        for x in range(100):
            tx_string = ""
            for val_name in xboxdrv_read_thread.controls:
                tx_string += (",%s" % (xboxdrv_read_thread.controls[val_name]))
            tx_string = tx_string.lstrip(',')

            tx_vals = tx_string
            tx_crc = hashlib.md5(tx_string.encode('utf-8')).hexdigest()
            tx_string =tx_string.lstrip(',') + "_" + hashlib.md5(tx_string.encode('utf-8')).hexdigest()


            rx_arr = tx_string.split("_")
            vals =rx_arr[0].strip()
            crc = rx_arr[1].strip()

            crc_match = False
            if tx_crc == (hashlib.md5(vals.encode('utf-8')).hexdigest()):
                crc_match = True
            #print("CRC Match:\t%r"%(crc_match))

            last_time = time.time() - xboxdrv_read_thread.last_data
            print("%.2f\t%.3f\tCRC MATCH: %r" % ( (xboxdrv_read_thread.controls['X1'] / 32768) , last_time,crc_match))


            time.sleep(0.1)


        xboxdrv_read_thread.stop()


    #Ctrl C
    except KeyboardInterrupt:
            print("User cancelled")

        #error
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    finally:
        #stop the controller
        pass
        xboxdrv_read_thread.stop()