#!/usr/bin/env python3
import sys
print("print python version:"+str(sys.version))
############################
#
#   This class is make a basic tx stream writer for the xbox controller
#   Handles basic consistent consistent tx frequency of messages sent
#   and basic error checking. TX radio callback function is used to send the data
#
############################


import threading
import select
import subprocess
import os
import time
import random
import hashlib
from xboxcnt.xboxdrv_wrapper import xboxdrv_reader
from termcolor import colored

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from xboxcnt.xbox_config import SEND_ORDER


class Xbox_RX_Man(xboxdrv_reader):
    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True
        self.running = False
        self.fresh_lineread = False
        self.lineread = ""
        self.last_data = 0.0
        self.functional = True

        self.expo_enable = True
        self.expo_controls = ['X1', 'X2', 'Y1', 'Y2']

        self.controls = {'X1': 0, 'Y1': 0, 'X2': 0, 'Y2': 0,
                         'du': 0, 'dd': 0, 'dl': 0, 'dr': 0,
                         'back': 0, 'guide': 0, 'start': 0,
                         'TL': 0, 'TR': 0,
                         'A': 0, 'B': 0, 'X': 0, 'Y': 0,
                         'LB': 0, 'RB': 0,
                         'LT': 0, 'RT': 0,
                         'du_T': 0, 'dd_T': 0, 'dl_T': 0, 'dr_T': 0,
                         'back_T': 0, 'guide_T': 0, 'start_T': 0,
                         'TL_T': 0, 'TR_T': 0,
                         'A_T': 0, 'B_T': 0, 'X_T': 0, 'Y_T': 0,
                         'LB_T': 0, 'RB_T': 0,
                         'idx': 0,
                         'last_read': time.time()
                         }
        self.running = True
        self.last_data = time.time()

        self.err_cnt = 0
        self.crc_fail = 0
        self.debug = False


#   Override run & stop functions due to no xboxdrv process running
    def run(self):
        pass
    def stop(self):
        pass


#
#   Main function called each time an RX message is received from radios
#   This function decodes the message & ensures data integrity via CRC

    def RX_callback(self,rx_msg):
        crc_match = False
        try:
            #print(rx_msg)
            #print(type(rx_msg))
            (vals,crc) =rx_msg.decode("utf-8").strip().split("_")

            if crc == (hashlib.md5(vals.encode('utf-8')).hexdigest()):
                crc_match = True
            else:
                crc_match = False
                self.crc_fail += 1
                return

            val_arr = vals.split(',')
            val_idx = 0
            #print(len(val_arr),len(self.controls),crc_match)

            #for val_name in self.controls:
            for val_name in SEND_ORDER:
                self.controls[val_name] = val_arr[val_idx]
                val_idx += 1

            self.controls['last_read'] = float(self.controls['last_read'])

            if self.debug:
                if not crc_match:
                    print(colored((self.controls['X1'],self.controls['Y1'],self.controls['X2'],self.controls['Y2'],crc_match), 'red'))
                else:
                    print(colored((self.controls['X1'], self.controls['Y1'], self.controls['X2'], self.controls['Y2'], crc_match),'green'))
            #print(self.controls['X1'],self.controls['Y1'],self.controls['X2'],self.controls['Y2'],crc_match)

            #print("CRC Match:\t%r" % (crc_match))

        except:
            print("CRC Match:\t%r" % (crc_match))
            self.err_cnt += 1





def write_message(message,stream_idx):
    #print(message)
    test_rx.RX_callback(message)
    pass

if __name__ == "__main__":
    try:
        test_tx = Xbox_TX_Man(tx_write_call=write_message)
        test_tx.start()

        test_rx = Xbox_RX_Man()
        test_rx.start()

        time.sleep(5)

        test_rx.stop()
        test_tx.stop()


    #Ctrl C
    except KeyboardInterrupt:
            print("User cancelled")

        #error
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    finally:
        #stop the controller
        pass
        test_rx.stop()
        test_tx.stop()
