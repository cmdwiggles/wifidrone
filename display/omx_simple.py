#!/usr/bin/env python3

import threading
import subprocess
import os
import sys
import time
import pexpect
from enum import Enum
import binascii

class Status(Enum):
    stopped = 0
    starting = 1
    running = 2
    error = 3

class omx_simple(threading.Thread):

    def __init__(self,omx_path="/opt/vc/src/hello_pi/hello_video/hello_video.bin"):
        threading.Thread.__init__(self)
        self.status = Status.starting
        self.running = False
        self.daemon = True

        self._omx_ps = None
        self._omx_path = omx_path

    def run(self):
        self.running = True
        self.status = Status.starting
        self._start()

    def _start(self):
        print("Starting omx ps")
        if not self._omx_ps:
            # self._omx_ps = pexpect.spawn(
            #     self._omx_path
            # )
            self._omx_ps = subprocess.Popen([self._omx_path],stdin=subprocess.PIPE,universal_newlines=False)

            if not self._omx_ps.poll():
                print("OMX player hello_video is starting")

    def write(self,write_str):
        if self.running and self._omx_ps:
            #if not self._omx_ps.poll():
            if True:
                #print(write_str)
                self._omx_ps.stdin.write(write_str)
                #self._omx_ps.stdin.write(write_str.strip())
                #self._omx_ps.stdin.write(binascii.a2b_qp(write_str))
                #
                self._omx_ps.stdin.flush()

                #print(self._omx_ps.communicate())

    def stop(self):
        print("Killing OMX player hello_video")
        self.running=False

        if self._omx_ps:
            if not self._omx_ps.poll():
                self._omx_ps.kill()

        self._omx_ps = None

    def __del__(self):
        self.running = False
        self.stop()






