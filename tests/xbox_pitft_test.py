#!/usr/bin/env python3
import time
from pitft.xboxscreen import PiTFT_Controller_Display
from xboxcnt.xboxdrv_wrapper import xboxdrv_reader
import os, sys
import threading

try:
    #print("test start")
    xboxdrv_read_thread = xboxdrv_reader()
    xboxdrv_read_thread.start()
    print("xboxdrv class started")
    test = PiTFT_Controller_Display(xboxdrv=xboxdrv_read_thread)
    test.start()
    print("pitft class started")
    while(True):
        #print(xboxdrv_read_thread.controls['X1'])
        time.sleep(0.05)

#Ctrl C
except KeyboardInterrupt:
        print("User cancelled")

    #error
except:
    print("Unexpected error:"), sys.exc_info()[0]
    raise

finally:
    xboxdrv_read_thread.stop()
    test.stop()