#!/usr/bin/env python3

import threading
import os
import sys
import time

import picamera

class MyOutput(object):
    def __init__(self):
        self.size = 0


    def write(self, s):
        self.size += len(s)
        print(len(s))

    def flush(self):
        print('%d bytes would have been written' % self.size)

with picamera.PiCamera() as camera:
    camera.resolution = (640, 480)
    camera.framerate = 60
    camera.start_recording(MyOutput(), format='h264')
    #camera.wait_recording(50)
    time.sleep(10)
    camera.stop_recording()