#!/usr/bin/env python3.4

'''

This module is designed to setup a simple test for loopback speed between 2 dongles on 1 pi2


'''

# Relative import hack
import sys
import os
PACKAGE_PARENT = '..'
sys.path.append(os.path.normpath(os.path.join(os.getcwd(), PACKAGE_PARENT)))



from comms.radio_enum import radio_enum
from comms.rx_control import rx_wifi_control
from comms.tx_control import tx_wifi_control
from xboxcnt.xbox_tx_manager import Xbox_TX_Man
from xboxcnt.xbox_rx_manager import Xbox_RX_Man

import time
from termcolor import colored

latency_buff_len = 100

latency_buff = [0.0] * latency_buff_len



def _test_callback(input_Str):
    try:
        #print(input_Str)
        tmp_arr = input_Str.split()
        latency_millis = (time.time() - float(tmp_arr[0])) * 1000
        #print("%.1f"%latency_millis)
        latency_buff.pop(0)
        latency_buff.append(latency_millis)


    except:
        pass
        print("failed convert")

def _test_callback_print(input_Str):
    print(input_Str)

def _test_callback_tx_print(input_Str,idx):
    print(input_Str)


def _calc_latency():
    total_latency = 0.0

    for lat_val in latency_buff:
        total_latency += lat_val

    return (total_latency / latency_buff_len)


if __name__ == "__main__":
    test_rx = None
    test_tx = None
    test_xbox_tx_man = None
    test_xbox_rx_man = None

    try:

    ################ Transmission Parameters ####################

        channel = 13                    #    Wifi channel to use
        port = 0                       #    Port to send & receive on
        tx_streams = 1                 #    How many parrallel TX streams to open
        block_size = 1                 #    Number of data packets in a block (default 8)
        fec_packets = 0                #    Number of FEC packets per block (default 4).
        packet_length = 512            #    Number of bytes per packet (default 1450. max 1450). This is also the FEC block size.
        retransmissions = 2            #    Number of transmissions of a block (default 1)
        retran_buffer_length = 1       #    Number of transmissions blocks that are buffered (default 1).
                                       #        This is needed in case of diversity if one adapter delivers data faster than the other. Note that this increases latency
        min_packet_bytes = 100         #    Minimum number of bytes per frame (default: 0)


    ################ ENUMERATE RADIOS #############################

        enum_radios = radio_enum()
        enum_radios.start()

        if not enum_radios.enum_complete:
            print("enumerating radios")
        while not enum_radios.enum_complete:
            time.sleep(0.01)

        print("Enumerate complete")
        print("Fast radios: %s"% (enum_radios.fast_tx))
        print("Slow radios: %s" % (enum_radios.slow_tx))

        rx_nics = enum_radios.slow_tx     #   Slow wifi dongle on test setup
        tx_nics = enum_radios.fast_tx     #   Fast wifi dongle on test setup


    ################# RX setup #########################
        test_rx = rx_wifi_control(wlan=rx_nics,channel=channel,
                                  port=port,block_size=block_size,
                                  fec_pac=fec_packets,packet_length=packet_length,
                                  retran_buff=retran_buffer_length)
        test_rx.start()

        while not test_rx.rx_ps_mon:
            print("waiting for rx pos to start")
            time.sleep(1)


################# Xbox RX manager setup #########################
        test_xbox_rx_man = Xbox_RX_Man()
        test_xbox_rx_man.start()
        #test_xbox_rx_man.debug = True
        test_rx.rx_ps_mon.callbacks.append(test_xbox_rx_man.RX_callback)

################# TX setup #########################
        test_tx = tx_wifi_control(wlan=tx_nics, channel=channel,
                                  fec_pac=fec_packets, packet_length=packet_length,
                                  port_start=port, block_size=block_size,
                                  retrans=retransmissions, min_bytes=min_packet_bytes,
                                  streams_ascii=tx_streams)
        test_tx.start()

        while not test_tx.tx_ps_mon:
            print("waiting for tx pos to start")
            time.sleep(1)

################# Xbox tx manager setup #########################

        test_xbox_tx_man = Xbox_TX_Man(tx_write_call=test_tx.write_message)
        test_xbox_tx_man.start()

####################################################################
        run = True
        while(run):
            loop_time = time.time()- (float(int(test_xbox_rx_man.controls['idx'])))/10000.0
            last_xbox_read = time.time()- float(test_xbox_rx_man.controls['last_read'])

            print_str = "Lag: %.3f\t Last Xbox: %.3f\tTx rate: %.1f" % (loop_time,last_xbox_read,test_tx.tx_ps_mon.packet_rate)
            print(colored(print_str, 'red'))

            time.sleep(1)

    # Ctrl C
    except KeyboardInterrupt:
        print("User cancelled")
        raise

        # error
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    finally:
        # stop the controller
        pass
        if test_rx:
            test_rx.stop()
        if test_tx:
            test_tx.stop()
        if test_xbox_tx_man:
            test_xbox_tx_man.stop()
        if test_xbox_rx_man:
            test_xbox_rx_man.stop()
