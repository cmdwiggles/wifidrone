#!/usr/bin/env python3

'''

This module is designed to setup a simple test for camera loopback between 2 dongles on 1 pi2


'''

from comms.rx_control import rx_wifi_control
from comms.tx_control import tx_wifi_control
from display.omx_simple import omx_simple
import time
import picamera
import binascii

def test_callback(test_in):
    #print(test_in)
    #print()
    #print(len(test_in))
    omx_player.write(test_in)

    pass


if __name__ == "__main__":

################ Transmission Parameters ####################

    rx_nics = ['wlan0']     #   Slow wifi dongle on test setup
    tx_nics = ['wlan1']     #   Fast wifi dongle on test setup

    channel = 1                    #    Wifi channel to use
    port = 0                       #    Port to send & receive on
    tx_streams_bin = 1                 #    How many parrallel TX streams to open
    tx_streams_ascii = 1
    block_size = 8                 #    Number of data packets in a block (default 8)
    fec_packets = 4                #    Number of FEC packets per block (default 4).
    packet_length = 1450            #    Number of bytes per packet (default 1450. max 1450). This is also the FEC block size.

    retransmissions = 1            #    Number of transmissions of a block (default 1)
    retran_buffer_length = 1       #    Number of transmissions blocks that are buffered (default 1).
                                   #        This is needed in case of diversity if one adapter delivers data faster than the other. Note that this increases latency
    min_packet_bytes = 0         #    Minimum number of bytes per frame (default: 0)



################# RX setup #########################
    print("################# TX setup #########################")

    test_rx = rx_wifi_control(wlan=rx_nics,channel=channel,
                              port=port,block_size=block_size,
                              fec_pac=fec_packets,packet_length=packet_length,
                              retran_buff=retran_buffer_length)
    test_rx.start()


    while not test_rx.rx_ps_mon:
        print("waiting for rx monitor to start")
        time.sleep(1)

    #test_rx.rx_ps_mon.callbacks.append(test_callback)



################# TX setup #########################
    print("################# TX setup #########################")
    test_tx = tx_wifi_control(wlan=tx_nics, channel=channel,
                              fec_pac=fec_packets, packet_length=packet_length,
                              port_start=port, block_size=block_size,
                              retrans=retransmissions, min_bytes=min_packet_bytes,
                              streams_ascii=tx_streams_ascii, streams_bin=tx_streams_bin
                              )

    test_tx.start()
    time.sleep(1)


################# OMX setup #########################
    print("################# OMX setup #########################")
    omx_player = omx_simple()
    omx_player.start()
    time.sleep(1)

    #test_rx.rx_ps_mon.callbacks.append(test_callback)


################# Camera setup #########################
    print("################# Camera setup #########################")
    camera = picamera.PiCamera()
    camera.resolution = (1480, 720)
    #camera.resolution = (1920, 1080)
    camera.framerate = 60
    #camera.start_recording(test_tx.tx_pipes_bin[(tx_streams_bin -1)], format='h264')
    camera.start_recording(omx_player, format='h264')
    print("sending video")
##########################################################


    while(True):
        time.sleep(1)
        #print(test_tx.tx_ps_mon.packet_rate)
        pass
