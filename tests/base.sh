#!/bin/bash
# rx script

#adapt these to your needs
VID_CHANNEL="13"
CONTROL_CHANNEL="1"

VID_NICS="wlan1"
CONTROL_NICS="wlan0"

WBC_PATH="/home/pi/wifibroadcast"
DISPLAY_PROGRAM="/opt/vc/src/hello_pi/hello_video/hello_video.bin" 


##################################
#change these only if you know what you are doing (and remember to change them on both sides)
BLOCK_SIZE=8
FECS=4
PACKET_LENGTH=1024
PORT=0
##################################

function prepare_nic {
	echo "updating wifi ($1, $2)"
	ifconfig $1 down
	iw dev $1 set monitor otherbss fcsfail
	ifconfig $1 up
	iwconfig $1 channel $2
}

################################# SCRIPT START #######################


# Make sure only root can run our script
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

#prepare NICS
for NIC in $VID_NICS
do
	prepare_nic $NIC $VID_CHANNEL
done

for NIC in $CONTROL_NICS
do
	prepare_nic $NIC $CONTROL_CHANNEL
done



	echo "Starting without recording"

	# echo "/home/pi/wifibroadcast/rx -p 0 -b 8 -r 4 -f 1024 wlan1 | /opt/vc/src/hello_pi/hello_video/hello_video.bin &"
	# echo "$WBC_PATH/rx -p $PORT -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH $VID_NICS | $DISPLAY_PROGRAM &"
	# echo "xboxdrv --quiet | /home/pi/wifibroadcast/tx -b 8 -r 4 -f 1024 wlan0"
	# echo "xboxdrv --quiet | $WBC_PATH/tx -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH $CONTROL_NICS"

#sudo ./rx -p 1 -b 4 wlan0
$WBC_PATH/rx -p 1 -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH $VID_NICS
$WBC_PATH/rx -p $PORT -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH $VID_NICS | $DISPLAY_PROGRAM &
#xboxdrv --quiet | $WBC_PATH/tx -b $BLOCK_SIZE -r $FECS -f $PACKET_LENGTH $CONTROL_NICS