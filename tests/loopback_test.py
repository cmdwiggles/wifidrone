#!/usr/bin/env python3


# Relative import hack
import sys
import os

if __name__ == "__main__":
    PACKAGE_PARENT = '..'
    sys.path.append(os.path.normpath(os.path.join(os.getcwd(), PACKAGE_PARENT)))

'''

This module is designed to setup a simple test for loopback speed between 2 dongles on 1 pi2


'''

from comms.radio_enum import radio_enum
from comms.rx_control import rx_wifi_control
from comms.tx_control import tx_wifi_control

import time

latency_buff_len = 100

latency_buff = [0.0] * latency_buff_len



def _test_callback(input_Str):
    try:
        #print(input_Str)
        tmp_arr = input_Str.split()
        latency_millis = (time.time() - float(tmp_arr[0])) * 1000
        #print("%.1f"%latency_millis)
        latency_buff.pop(0)
        latency_buff.append(latency_millis)


    except:
        pass
        print("failed convert")

def _calc_latency():
    total_latency = 0.0

    for lat_val in latency_buff:
        total_latency += lat_val

    return (total_latency / latency_buff_len)



if __name__ == "__main__":

################ Transmission Parameters ####################

    # rx_nics = ['wlan1']     #   Slow wifi dongle on test setup
    # tx_nics = ['wlan0']     #   Fast wifi dongle on test setup

    channel = 13                    #    Wifi channel to use
    port = 0                       #    Port to send & receive on
    tx_streams = 1                 #    How many parrallel TX streams to open
    block_size = 2                 #    Number of data packets in a block (default 8)
    fec_packets = 2                #    Number of FEC packets per block (default 4).
    packet_length = 256            #    Number of bytes per packet (default 1450. max 1450). This is also the FEC block size.
    packet_padding_len = 100       #
    retransmissions = 2            #    Number of transmissions of a block (default 1)
    retran_buffer_length = 1       #    Number of transmissions blocks that are buffered (default 1).
                                   #        This is needed in case of diversity if one adapter delivers data faster than the other. Note that this increases latency
    min_packet_bytes = 100         #    Minimum number of bytes per frame (default: 0)


################ ENUMERATE RADIOS #############################

    enum_radios = radio_enum()
    enum_radios.start()

    if not enum_radios.enum_complete:
        print("enumerating radios")
    while not enum_radios.enum_complete:
        time.sleep(0.01)

    print("Enumerate complete")
    print("Fast radios: %s"% (enum_radios.fast_tx))
    print("Slow radios: %s" % (enum_radios.slow_tx))

    rx_nics = enum_radios.slow_tx     #   Slow wifi dongle on test setup
    tx_nics = enum_radios.fast_tx     #   Fast wifi dongle on test setup


################# RX setup #########################
    test_rx = rx_wifi_control(wlan=rx_nics,channel=channel,
                              port=port,block_size=block_size,
                              fec_pac=fec_packets,packet_length=packet_length,
                              retran_buff=retran_buffer_length)
    test_rx.start()

    while not test_rx.rx_ps_mon:
        print("waiting for rx pos to start")
        time.sleep(1)
    test_rx.rx_ps_mon.callbacks.append(_test_callback)


################# TX setup #########################
    test_tx = tx_wifi_control(wlan=tx_nics, channel=channel,
                              fec_pac=fec_packets, packet_length=packet_length,
                              port_start=port, block_size=block_size,
                              retrans=retransmissions, min_bytes=min_packet_bytes,
                              streams_ascii=tx_streams,debug=True)

    test_tx.start()

    time.sleep(5)
    Run = True
    while(Run):
        for x in range(100):
            fail = False
            send_str = ("%s " %(time.time())).ljust(packet_padding_len,"#") + "\n"
            write_status = test_tx.write_message(send_str,0,ascii=False)

            if write_status:
                pass
                time.sleep(0.005)
            else:
                fail = True
                #Run = False
                #print("######## ERROR TEST FAILED ##########")
                break
        print("TX rate: %.1f Avg Latency: %.2f RSSI dbm %.2f\tFail:\t%r"%(test_tx.tx_ps_mon.packet_rate,_calc_latency(),test_rx.rx_ps_mon.card_rx_dbm[0],fail))
