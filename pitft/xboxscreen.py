#!/usr/bin/python2.7
import threading
import pygame
from pygame.locals import *
import os
import time



class PiTFT_Controller_Display(threading.Thread):

    def __init__(self,xboxdrv=None):
        threading.Thread.__init__(self)
        os.putenv('SDL_VIDEODRIVER', 'dummy')
        os.putenv('SDL_FBDEV', '/dev/fb1')
        os.putenv('SDL_MOUSEDRV', 'TSLIB')
        os.putenv('SDL_MOUSEDEV', '/dev/input/touchscreen')
        pygame.init()
        self.daemon = True
        self.running = False
        self.cntrl_outline =Color("green")
        self.cntrl_line = Color("red")
        self.trigger_line = Color("blue")
        self.last_read_font = pygame.font.Font(None,20)

        self.lcd = pygame.display.set_mode((320, 240))
        pygame.mouse.set_visible(False)
        pygame.display.update()

        self.xboxdrv = xboxdrv
        self.draw_xbox_outputs = False
        if self.xboxdrv:
            if self.xboxdrv.functional:
                self.draw_xbox_outputs = True

        self.draw_boxes(update=True)
        print("PITFT init done")

    def run(self):
        self.running = True
        self._start()

    def _start(self):
        while self.running:
            self.lcd.fill([0,0,0])
            self.draw_boxes()
            self.draw_control_lines()
            self.draw_trigger_lines()
            self.draw_last_read()
            pygame.display.update()
            time.sleep(0.01)

    def draw_boxes(self,update=False):
        pygame.draw.ellipse(self.lcd,self.cntrl_outline,[5,85,150,150],2)
        pygame.draw.ellipse(self.lcd,self.cntrl_outline,[165,85,150,150],2)
        if update:
            pygame.display.update()

    def draw_control_lines(self):
        X1 = round((self.xboxdrv.controls['X1'] / 32768) * 75)
        Y1 = round((self.xboxdrv.controls['Y1'] / 32768) * -75)
        X2 = round((self.xboxdrv.controls['X2'] / 32768) * 75)
        Y2 = round((self.xboxdrv.controls['Y2'] / 32768) * -75)
        pygame.draw.line(self.lcd,self.cntrl_line,[80,160],[X1+80,Y1+160])
        pygame.draw.line(self.lcd,self.cntrl_line,[240,160],[X2+240,Y2+160])

    def draw_trigger_lines(self):
        left_trig = round((self.xboxdrv.controls['LT'] / 255) * 150)
        right_trig = round((self.xboxdrv.controls['RT'] / 255) * 150)
        pygame.draw.line(self.lcd,self.trigger_line,[1,240],[1,240-left_trig])
        pygame.draw.line(self.lcd,self.trigger_line,[319,240],[319,240-right_trig])

    def draw_last_read(self):
        millis_since_last_read = round((time.time() -self.xboxdrv.controls['last_read']) * 1000)
        if millis_since_last_read < 1000:
            disp_text = '%d'%millis_since_last_read
        else:
            disp_text = "LATE"

        text_surface = self.last_read_font.render(disp_text, True, Color("white"))
        rect = text_surface.get_rect(right=320,width=160)
        self.lcd.blit(text_surface, rect)

    def stop(self):
        self.running = False

    def __del__(self):
        self.stop()


if __name__ == "__main__":
    test_PiTFT = PiTFT_Controller_Display()
    time.sleep(5)