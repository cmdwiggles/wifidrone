#!/usr/bin/env python3
import subprocess
import os
import sys
import time


###############################
#
#	wlan0	RT2870/RT3070
#	wlan1	AR9271
#
###############################
CONTROL_BLOCK_SIZE=1			# -b
CONTROL_FECS=0					# -r
CONTROL_PACKET_LENGTH=512		# -f
CONTROL_PORT=1					# -p
CONTROL_MIN_PACKET_LENGTH=128	# - m

CONTROL_CHANNEL = 1
CONTROL_NICS="wlan1"
CONTROL_PORTS=1
TX_PIPES=[]

###############################
VID_BLOCK_SIZE=8
VID_FECS=4
VID_PACKET_LENGTH=1024
VID_PORT=0

VID_CHANNEL = 13
VID_NICS="wlan0"



################################
WBC_PATH_TX="/home/pi/wifibroadcast/tx"
WBC_PATH_RX="/home/pi/wifibroadcast/rx"
DISPLAY_PROGRAM="/opt/vc/src/hello_pi/hello_video/hello_video.bin" 

################################
def getCPUtemperature():
    res = os.popen('vcgencmd measure_temp').readline()
    return(res.replace("temp=","").replace("'C\n",""))

def prep_radio(NIC,CHANNEL):
	subprocess.call(['ifconfig',NIC,'down'])
	subprocess.call(['iw','dev',NIC,'set','monitor','otherbss','fcsfail'])
	subprocess.call(['ifconfig',NIC,'up'])
	subprocess.call(['iwconfig',NIC,'channel',str(CHANNEL)])


################################
# os.environ["SDL_FBDEV"] = "/dev/fb1"

if os.getuid() != 0:
    print("Not sudo user,\nExiting")
    sys.exit()

print("Core temperature:\t" +getCPUtemperature())
print(sys.version)
start = time.time()




################################
skip = False
if not skip:
	print("python wifi control setup starting")

	for nic in CONTROL_NICS.split():
		print("Setting control card: %s to channel %d\t\t" % (nic, CONTROL_CHANNEL)),
		prep_radio(nic,CONTROL_CHANNEL)
		print("Success")

	for nic in CONTROL_NICS.split():
		print("Setting video card: %s to channel %d\t\t" % (VID_NICS, VID_CHANNEL)),
		prep_radio(VID_NICS,VID_CHANNEL)
		print("Success")

################################
try:

	video_rx_ps = subprocess.Popen([WBC_PATH_RX,'-p',str(VID_PORT),'-b',str(VID_BLOCK_SIZE),\
		'-r',str(VID_FECS),'-f',str(VID_PACKET_LENGTH),VID_NICS],\
		stdout=subprocess.PIPE)

	video_display = subprocess.Popen([DISPLAY_PROGRAM], stdin=video_rx_ps.stdout)

	xbox_cont = subprocess.Popen(['./xboxdrv_wrapper.py'], stdout=subprocess.PIPE)
	#xbox_cont = subprocess.Popen(['xboxdrv','--quiet','--detach-kernel-driver'],stdout=subprocess.PIPE)

	control_tx_ps = subprocess.Popen([WBC_PATH_TX,'-b',str(CONTROL_BLOCK_SIZE),\
		'-r',str(CONTROL_FECS),'-f',str(CONTROL_PACKET_LENGTH),'-s',str(CONTROL_PORTS),'-p',str(CONTROL_PORT),'-m',str(CONTROL_MIN_PACKET_LENGTH),CONTROL_NICS],stdin=xbox_cont.stdout)
	

	# for pipe_idx in range(CONTROL_PORTS):
	# 	pipe_path = "/tmp/fifo"+str(pipe_idx)
	# 	subprocess.call(['chmod','775',pipe_path])
	# 	TX_PIPES.append(open(pipe_path,'w'))

	while True:
		time.sleep(1)

except:
	print("Killing sub processes")
	video_rx_ps.kill()
	video_display.kill()
	xbox_cont.kill()
	control_tx_ps.kill()
	raise


# for x in range(CONTROL_PORTS):
# 		for y in range(100):
# 			TX_PIPES[x].write("TEFGSFDGFDGLSFDGLMDFSMOHFDOG<DFPHGMHFMHsauiofgnasdgubnaripsdufnasdpgbnasogmna[hjsfdoihmnfidohjaeopifgmnsdoiignfidogj[")
# 			#print(xboxCont.PyGameAxis.LTHUMBY)
# 			time.sleep(0.001)
# #time.sleep(5)


#time.sleep(5)
# end =time.time()
# print((end - start) * 1000)






#############################
print("Killing sub processes")
video_rx_ps.kill()
video_display.kill()
xbox_cont.kill()
control_tx_ps.kill()

for x in range(CONTROL_PORTS):
	try:
		TX_PIPES[x].close()
	except:
		print("Error closing pipe %d, possibly already closed or broken"%(x))
#############################