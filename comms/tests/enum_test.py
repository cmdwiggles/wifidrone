#!/usr/bin/env python3


# Relative import hack
import sys
import os
PACKAGE_PARENT = '..'
sys.path.append(os.path.normpath(os.path.join(os.getcwd(), PACKAGE_PARENT)))

from radio_enum import radio_enum
import time

try:
    start_time = time.time()
    test = radio_enum()
    test.start()

    while not test.enum_complete:
        #print("testT:\t%.2f"%(time.time() - start_time))
        time.sleep(0.0001)

    print("testT:\t%.4f" % (time.time() - start_time))
    print(test.fast_tx)
    print(test.slow_tx)


#Ctrl C
except KeyboardInterrupt:
    print("User cancelled")

    #error
except:
    print("Unexpected error:", sys.exc_info()[0])
    raise

finally:
    #stop the controller
    test.stop()