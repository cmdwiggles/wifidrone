#!/usr/bin/env python3

# Relative import hack
import sys
import os
PACKAGE_PARENT = '..'
sys.path.append(os.path.normpath(os.path.join(os.getcwd(), PACKAGE_PARENT)))

from radio_enum import radio_enum
from tx_control import tx_wifi_control
import time

try:
    start_time = time.time()
    test_enum = radio_enum()
    test_enum.start()

    while not test_enum.enum_complete:
        time.sleep(0.00001)



    test = tx_wifi_control(wlan=test_enum.fast_tx, channel=1)  # Fast wifi dongle on test setup
    # test = tx_wifi_control(wlan=['wlan0'],channel=1)    #   Slow wifi dongle on test setup
    test.start()

    time.sleep(2)

    while (True):
        for x in range(1000):
            test_Act_str = ("write cnt: %d \n" % x)
            write_status = test.write_str(test_Act_str, 0)
            # print(write_status)
            #sys.exit()
            if write_status:
                time.sleep(0.001)
                # print(time.time())
            else:

                #print("######## ERROR TEST FAILED ##########")
                break
        #print("TX rate: %.1f" % (test.tx_ps_mon.packet_rate))

    print("Test finished!")


# Ctrl C
except KeyboardInterrupt:
    print("User cancelled")

    # error
except:
    print("Unexpected error:", sys.exc_info()[0])
    raise

finally:
    # stop the controller
    test_enum.stop()
    test.stop()
    pass