#!/usr/bin/env python3

import threading
import subprocess
import os
import sys
import time
import xml.etree.ElementTree as etree
from configobj import ConfigObj

class radio_enum(threading.Thread):
    def __init__(self,config_path="/tmp/radio_enum.cfg"):
        threading.Thread.__init__(self)
        self.running = False
        self.daemon = True
        self.enum_complete = False
        self.__config_path = config_path
        self.radio_config = ConfigObj()

        self.fast_tx_types = ['ath9k_htc']

        self.fast_tx = []
        self.slow_tx = []



    def run(self):
        self.running = True
        self._start()

    def _start(self):
        enum_found = False
        print("Searching for existing radio config file %s"%self.__config_path)
        if os.path.isfile(self.__config_path):
            print("Existing radio enum config file, parsing...")
            try:
                self.radio_config = ConfigObj(self.__config_path)
                enum_found = True
                self.__read_radio_config()
            except IOError:
                print("Could not parse config file")
            except:
                print("unspecified error")


        if not enum_found:
            print("starting radio hardware listing")
            lshw_str_out = os.popen('lshw -quiet -c network -xml').read()
            adapter_root = etree.fromstring(lshw_str_out)

            for child in adapter_root:
                fast_tx_dev = False
                adapter_name = None
                for sub_child in child:
                    if sub_child.tag == "description":
                        if "Wireless interface" != sub_child.text:
                            break
                    if sub_child.tag == "logicalname":
                        adapter_name = sub_child.text

                    if sub_child.tag == "configuration":
                        for sub_sub_child in sub_child:
                            if sub_sub_child.attrib['id'] == "driver":
                                if sub_sub_child.attrib['value'] in self.fast_tx_types:
                                    fast_tx_dev = True

                if adapter_name:
                    if fast_tx_dev:
                        if not adapter_name in self.fast_tx:
                            self.fast_tx.append(adapter_name)
                    elif not adapter_name in self.slow_tx:
                        self.slow_tx.append(adapter_name)


            enum_found = True
            self.__write_radio_config()
        self.enum_complete = True
        if enum_found:
            print("Radio enum Success")
        else:
            print("Error, radio enum unable to complete")


    def __write_radio_config(self):
        self.radio_config['fast_tx'] = self.fast_tx
        self.radio_config['slow_tx'] = self.slow_tx
        try:
            self.radio_config.filename = self.__config_path
            self.radio_config.write()
            print("Success! config file wrote")
        except:
            print("Error, unable to write config")



    def __read_radio_config(self):
        if self.radio_config:
            self.fast_tx = self.radio_config['fast_tx']
            self.slow_tx = self.radio_config['slow_tx']

        else:
            print("Error, unable to read radio config")

    def stop(self):
        self.running = False

    def __del__(self):
        self.stop()
