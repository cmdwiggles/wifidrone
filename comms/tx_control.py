#!/usr/bin/env python3

'''
From pi 29/02/2016

pi@basestation ~/basestationsidewificontrol/comms $ /home/pi/wifibroadcast/tx
Raw data transmitter (c) 2015 befinitiv  GPL2
(c)2015 befinitiv. Based on packetspammer by Andy Green.  Licensed under GPL2

Usage: tx [options] <interface>

Options
-r <count> Number of FEC packets per block (default 4). Needs to match with rx.

-f <bytes> Number of bytes per packet (default 1450. max 1450). This is also the FEC block size. Needs to match with rx
-p <port> Port number 0-255 (default 0)
-b <count> Number of data packets in a block (default 8). Needs to match with rx.
-x <count> Number of transmissions of a block (default 1)
-m <bytes> Minimum number of bytes per frame (default: 0)
-s <stream> If <stream> is > 1 then the parameter changes "tx" input from stdin to named fifos. Each fifo transports a stream over a different port (starting at -p port and incrementing). Fifo names are "/tmp/fifo%d". (default 1)
Example:
  iwconfig wlan0 down
  iw dev wlan0 set monitor otherbss fcsfail
  ifconfig wlan0 up
  iwconfig wlan0 channel 13
  tx wlan0        Reads data over stdin and sends it out over wlan0

'''

import threading
import subprocess
import os
import sys
import time
import pexpect
from enum import Enum
from termcolor import colored

class Status(Enum):
    stopped = 0
    starting = 1
    running = 2
    error = 3




class tx_wifi_control(threading.Thread):
    def __init__(self, wlan=None, channel=1,
                 wbc_path_tx="/home/eoin/wifibroadcast/tx",
                 fec_pac=4,  # -r <count> Number of FEC packets per block (default 4). Needs to match with rx.
                 packet_length=1450,  # -f <bytes> Number of bytes per packet (default 1450. max 1450). This is also the FEC block size. Needs to match with rx
                 port_start=0,  # -p <port> Port number 0-255 (default 0)
                 block_size=8,  # -b <count> Number of data packets in a block (default 8). Needs to match with rx.
                 retrans=1,  # -x <count> Number of transmissions of a block (default 1)
                 min_bytes=0,  # -m <bytes> Minimum number of bytes per frame (default: 0)
                 streams_bin=1,  # -m <bytes> Minimum number of bytes per frame (default: 0)
                 streams_ascii=1,                                # -s <stream> number of streams, manually set to over 1 to enable pipes
                debug=False
                 ):

        self.status = Status.starting
        threading.Thread.__init__(self)

        self.running = False
        self.daemon = True

        self._wlan = wlan
        self._channel = channel
        self._wbc_path_tx = wbc_path_tx

        self._fec_lac = fec_pac
        self._packet_length = packet_length
        self._port_start = port_start
        self._block_size = block_size
        self._retrans = retrans
        self._min_bytes = min_bytes
        self._streams_ascii = streams_ascii
        self._streams_bin = streams_bin

        self.tx_ps = None
        self.tx_pipes_ascii = []
        self.tx_pipes_bin = []

        self.tx_ps_mon = None

        self.debug_on = debug


    def run(self):
        self.running = True
        self._start()

    def stop(self):
        print("Killing TX comm")
        self.running = False

        if self.tx_ps_mon:
            self.tx_ps_mon.stop()

        for tx_pipe in self.tx_pipes_ascii:
            try:
                if tx_pipe:
                    if not tx_pipe.closed:
                        tx_pipe.close()
                print("Closed pipe")
            except:
                print("Error closing pipe, possibly already closed or broken")

        for tx_pipe in self.tx_pipes_bin:
            try:
                if tx_pipe:
                    if not tx_pipe.closed:
                        tx_pipe.close()
                print("Closed pipe")
            except:
                print("Error closing pipe, possibly already closed or broken")

        if self.tx_ps:
            if not self.tx_ps.isalive():
                self.tx_ps.terminate()
        self.tx_ps = None


    def __del__(self):
        self.running = False
        self.stop()

    def _start(self):
        if self._wlan and self._channel:
            print("Starting TX controller %s on channel %d" % (self._wlan, self._channel))
            if not self._prep_radio(self._wlan, self._channel):
                self.running = False

            self._start_tx_ps()

        #self.running = False

        # while self.running:
        #     print("tx ps:" + str(self.tx_ps.poll()))
        #     time.sleep(0.01)


    def restart(self):
        print("Restarting wifi TX")
        self.stop()
        self._start()


    def _start_tx_ps(self):
        self._prep_pipes()
        if not self.tx_ps:

            self.tx_ps = pexpect.spawn(self._wbc_path_tx, [
                                           '-r',str(self._fec_lac),
                                           '-f',str(self._packet_length),
                                           '-p',str(self._port_start),
                                           '-b',str(self._block_size),
                                           '-x',str(self._retrans),
                                           '-m',str(self._min_bytes),
                                           '-s',str(self._streams_ascii+self._streams_bin)] +
                                       self._wlan)


            if self.tx_ps.isalive():
                print("TX process starting")


        print("starting TX ps monitor")
        self.tx_ps_mon = tx_ps_mon(tx_ps=self.tx_ps)
        self.tx_ps_mon.start()

        print("Setting up FiFo pipes")
        time.sleep(0.5)

        self._open_pipes()




    def _prep_radio(self,NICS,CHANNEL):
        err_code = 0
        for NIC in NICS:
            err_code += subprocess.call(['ifconfig',NIC,'down'])
            err_code += subprocess.call(['iw','dev',NIC,'set','monitor','otherbss','fcsfail'])
            err_code += subprocess.call(['ifconfig',NIC,'up'])
            err_code += subprocess.call(['iwconfig',NIC,'channel',str(CHANNEL)])

            if err_code > 0:
                print("Error initializing Wifi radio:\t%s"%NIC)
                return False

        if err_code == 0:
            for NIC in NICS:
                print("success radio %s on channel %d"%(NIC,CHANNEL))
        return True

    def _prep_pipes(self):
        existing_pipes = os.listdir("/tmp/")
        for pipe_idx in range(self._port_start, (self._streams_ascii+self._streams_bin)):

            fifo_name = "fifo%d"%(pipe_idx)
            pipe_path = "/tmp/fifo"+str(pipe_idx)
            print("pipe prep %s"%pipe_path)
            if not fifo_name in existing_pipes:
                print("making fifo:\t%s"%fifo_name)
                os.mkfifo(pipe_path)

            subprocess.call(['sudo','chmod','777',pipe_path])
            time.sleep(0.5)


    def _open_pipes(self):
        count = 0
        for pipe_idx in range(self._port_start, (self._streams_ascii+self._streams_bin)):
            pipe_path = "/tmp/fifo"+str(pipe_idx)
            if count <= self._streams_ascii:
                self.tx_pipes_bin.append(open(pipe_path,'wb'))
                print("opened bin pipe:\t%s"%pipe_path)
            else:
                self.tx_pipes_ascii.append(open(pipe_path,'w'))
                print("opened ascii pipe:\t%s"%pipe_path)
            count += 1


    def write_message(self,message,stream_idx,ascii=True,flush=True):
        if self.running:

            if self.tx_ps.isalive():
                if self.debug_on:
                    print(colored("write message_step 0", 'yellow'))
                if ascii:
                    if self.debug_on:
                        print(colored("write ascii message_step 1", 'yellow'))
                        print("Limits: %d to %d, this stream: %s",(0,len(self.tx_pipes_ascii),stream_idx))
                    if stream_idx >= 0 and stream_idx < len(self.tx_pipes_ascii):
                        if self.debug_on:
                            print(colored("write ascii message_step 2", 'yellow'))
                        if message:
                            if self.tx_pipes_ascii[stream_idx].closed:
                                print("closed pipe")
                                self.stop()
                            else:
                                try:
                                    self.tx_pipes_ascii[stream_idx].write(message.encode('utf-8'))
                                    if self.debug_on:
                                        print(colored("write ascii message_step 3", 'yellow'))
                                        print(colored(message.encode('utf-8'), 'yellow'))

                                    if flush:
                                        self.tx_pipes_ascii[stream_idx].flush()
                                    return True
                                except:
                                    print("Unexpected error:", sys.exc_info()[0])
                                    pass
                                    #print("Error writing to ascii tx pipe on stream:%d"%stream_idx)
                else:
                    if self.debug_on:
                        print(colored("write message_step 1", 'yellow'))
                        print("Limits: %d to %d, this stream: %s", (0, len(self.tx_pipes_ascii), stream_idx))
                    if stream_idx >= 0 and stream_idx < len(self.tx_pipes_bin):
                        if self.debug_on:
                            print(colored("write message_step 2", 'yellow'))
                        if message:
                            if self.tx_pipes_bin[stream_idx].closed:
                                print("closed pipe")
                                self.stop()
                            else:
                                #print(self.tx_pipes_bin)
                                self.tx_pipes_bin[stream_idx].write(message.encode('utf-8'))
                                try:
                                    #print(colored("write message", 'yellow'))
                                    #self.tx_pipes_bin[stream_idx].write(message)
                                    if flush:
                                        self.tx_pipes_bin[stream_idx].flush()
                                    return True
                                except:
                                    print("Unexpected error:", sys.exc_info()[0])
                                    pass
                                    #print("Error writing to tx pipe on stream: %d"%stream_idx)

        return False

    def write_ascii(self,message,stream_idx,flush=True):
        return self.write_message(message,stream_idx,ascii=True,flush=flush)

    def write_str(self, message, stream_idx, flush=True):
        return self.write_message(message, stream_idx, ascii=False, flush=flush)


class tx_ps_mon(threading.Thread):
    def __init__(self,tx_ps=None):
        threading.Thread.__init__(self)
        self.running = False
        self.daemon = True

        self.tx_ps = tx_ps

        self.packet_sent = 0
        self.packet_rate = 0.0

    def run(self):
        self.running = True
        self._start()

    def _start(self):

        while self.running:
            pass
            if self.tx_ps.isalive:
                tx_ps_str = self.tx_ps.readline()
                # print(colored("%s:\t%s" % (__name__, tx_ps_str), 'blue'))
                if tx_ps_str:
                    tx_ps_arr = tx_ps_str.strip().split()

                    #   Filter out data packet send rate
                    if len(tx_ps_arr) == 7:
                        #print(tx_ps_arr[1:4], ['data', 'packets', 'sent'])
                        if tx_ps_arr[0].isdigit() and tx_ps_arr[1:4] == [b'data', b'packets', b'sent']:
                            self.packet_sent = int(tx_ps_arr[0])
                            self.packet_rate = float(tx_ps_arr[6][:-1])
                            #print(self.packet_rate)

                #print("out:\t%s"%tx_ps_arr)



    def stop(self):
        self.running = False

    def __del__(self):
        self.stop()
