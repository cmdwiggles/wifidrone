#!/usr/bin/env python3

'''
From pi 29/02/2016

pi@basestation ~ $ sudo /home/pi/wifibroadcast/rx -h
(c)2015 befinitiv. Based on packetspammer by Andy Green.  Licensed under GPL2

Usage: rx [options] <interfaces>

Options
-p <port> Port number 0-255 (default 0)
-b <count> Number of data packets in a block (default 8). Needs to match with tx.
-r <count> Number of FEC packets per block (default 4). Needs to match with tx.

-f <bytes> Number of bytes per packet (default 1450. max 1450). This is also the FEC block size. Needs to match with tx
-d <blocks> Number of transmissions blocks that are buffered (default 1). This is needed in case of diversity if one adapter delivers data faster than the other. Note that this increases latency
Example:
  iwconfig wlan0 down
  iw dev wlan0 set monitor otherbss fcsfail
  ifconfig wlan0 up
  iwconfig wlan0 channel 13
  rx wlan0        Receive raw packets on wlan0 and output the payload to stdout


'''

import threading
import subprocess
import os
import sys
import time
import pexpect
from termcolor import colored


class rx_wifi_control(threading.Thread):
    def __init__(self,wlan=None,channel=1,
                 wbc_path_rx="/home/eoin/wifibroadcast/rx",
                 port=0,                                    # -p <port> Port number 0-255 (default 0)
                 block_size=8,                              # -b <count> Number of data packets in a block (default 8). Needs to match with tx.
                 fec_pac=4,                                 # -r <count> Number of FEC packets per block (default 4). Needs to match with tx.
                 packet_length=1450,                        # -f <bytes> Number of bytes per packet (default 1450. max 1450). This is also the FEC block size. Needs to match with tx
                 retran_buff=1,                              # -d <blocks> Number of transmissions blocks that are buffered (default 1). This is needed in case of diversity if one adapter delivers data faster than the other. Note that this increases latency
                 debug=False
                    ):

        threading.Thread.__init__(self)

        self.running = False
        self.daemon = True

        self.wlan = wlan
        self.channel = channel
        self.wbc_path_rx = wbc_path_rx

        self.fec_lac = fec_pac
        self.packet_length = packet_length
        self.port = port
        self.block_size = block_size
        self.retran_buff = retran_buff


        self.rx_ps = None
        self.rx_pipes = None

        self.rx_ps_mon = None
        self.debug = debug



    def run(self):
        self.running = True
        self._start()

    def stop(self):
        print("Killing RX comm")
        self.running = False

        if self.rx_ps_mon:
            self.rx_ps_mon.stop()


        if self.rx_ps:
            if self.rx_ps.isalive():
                self.rx_ps.terminate()
        self.rx_ps = None


    def __del__(self):
        self.running = False
        self.stop()

    def _start(self):
        if self.wlan and self.channel:
            print("Starting rx controller %s on channel %d"%(self.wlan,self.channel))
            if not self._prep_radio(self.wlan,self.channel):
                self.running = False

            self._start_rx_ps()




    def restart(self):
        print("Restarting wifi RX")
        self.stop()
        self._start()


    def _start_rx_ps(self):
        if not self.rx_ps:
            self.rx_ps = pexpect.spawn(self.wbc_path_rx,
                                           ['-p',str(self.port),
                                            '-b',str(self.block_size),
                                            '-r',str(self.fec_lac),
                                            '-f',str(self.packet_length),
                                            '-d',str(self.retran_buff),
		                                    ]+self.wlan)

            self.rx_ps.timeout=300


            if self.rx_ps.isalive():
                print("RX process starting")

        time.sleep(1)
        print("starting RX ps monitor")
        self.rx_ps_mon = rx_ps_mon(rx_ps=self.rx_ps,wlans=self.wlan,debug=self.debug)
        self.rx_ps_mon.start()




    def _prep_radio(self,NICS,CHANNEL):
        err_code = 0
        for NIC in NICS:
            err_code += subprocess.call(['ifconfig',NIC,'down'])
            err_code += subprocess.call(['iw','dev',NIC,'set','monitor','otherbss','fcsfail'])
            err_code += subprocess.call(['ifconfig',NIC,'up'])
            err_code += subprocess.call(['iwconfig',NIC,'channel',str(CHANNEL)])

            if err_code > 0:
                print("Error initializing Wifi radio:\t%s"%NIC)
                return False

        if err_code == 0:
            for NIC in NICS:
                print("success radio %s on channel %d"%(NIC,CHANNEL))
        return True






class rx_ps_mon(threading.Thread):
    def __init__(self,rx_ps=None,wlans=[],read_callbacks=[],debug=False):
        threading.Thread.__init__(self)
        self.running = False
        self.daemon = True

        self.rx_ps = rx_ps
        self.wlans=wlans

        self.card_rx_dbm = [0]*len(self.wlans)
        self.read_str=""
        self.callbacks=read_callbacks

        self.debug_on = debug

    def run(self):
        self.running = True
        self._start()

    def _start(self):
        while self.running:
            pass
            if self.rx_ps.isalive:
                rx_ps_str = self.rx_ps.readline()
                if self.debug_on:
                    print(colored("%s:\t%s"%(__name__,rx_ps_str),'red'))
                if rx_ps_str:
                    try:
                        if rx_ps_str.startswith(b'Signal (card '):
                            dbm_str = rx_ps_str[13:].strip()
                            card_id = int(dbm_str[:1])
                            dbm = int(dbm_str[3:-3])
                            self.card_rx_dbm[card_id] = dbm

                        elif rx_ps_str.startswith(b'TX RESTART: Detected blk'):
                            print(colored(rx_ps_str, 'yellow'))
                            pass
                        elif rx_ps_str.startswith(b'Could not fully reconstruct block'):
                            print(colored(rx_ps_str, 'blue'))
                            pass

                        else:
                            self.read_str = rx_ps_str
                            if self.callbacks:
                                for callback in self.callbacks:
                                    try:
                                        callback(self.read_str)
                                    except:
                                        print("RX POS MON callback failed")



                    except:
                        print("Failed read of RX monitor")
                        pass
                        #print(str(rx_ps_str))



    def stop(self):
        self.running = False

    def __del__(self):
        self.stop()





def _test_callback(input_Str):
    print(input_Str)

if __name__ == "__main__":

    try:
        start_time = time.time()
        #test = rx_wifi_control(wlan=['wlan1'],channel=1)    #   Fast wifi dongle on test setup
        test = rx_wifi_control(wlan=['wlan0'],channel=1)    #   Slow wifi dongle on test setup
        test.start()
        while not test.rx_ps_mon:
            print("waiting for rx pos to start")
            time.sleep(1)

        test.rx_ps_mon.callbacks.append(_test_callback)


        time.sleep(20)


        print("Test finished!")


    #Ctrl C
    except KeyboardInterrupt:
        print("User cancelled")

        #error
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise

    finally:
        pass
        #stop the controller
    test.stop()